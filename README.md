Perfect Grid
============

This is a windowing grid for the window snapping program [GridMove](http://jgpaiva.dcmembers.com/gridmove.html) for Microsoft Windows. I used the [grid making tutorial by Tyinsar](http://www.donationcoder.com/forum/index.php?topic=11534.0) to make the grid.

###### Installation
Copy the file [Perfect.grid](Perfect.grid) to the `Grids` folder in your GridMove installation directory, usually found at

    C:\Program Files (x86)\GridMove\Grids

###### Screenshot
![(screenshot)](screenshot.png "Screenshot of the grid")

###### Explanation of the grid numbers
0. **Top-left corner**<br />
   The window fills the top-left quadrant of the current monitor
0. **Top**<br />
   The window is maximized in the current monitor
0. **Top-right corner**<br />
   The window fills the top-right quadrant of the current monitor
0. **Top-left**<br />
   The window fills the top-left quadrant of the current monitor
0. **Top-right**<br />
   The window fills the top-right quadrant of the current monitor
0. **Middle-left**<br />
   The window fills the left half of the current monitor
0. **Middle-right**<br />
   The window fills the right half of the current monitor
0. **Bottom-left**<br />
   The window fills the bottom-left quadrant of the current monitor
0. **Bottom-right**<br />
   The window fills the bottom-right quadrant of the current monitor
0. **Bottom-left corner**<br />
   The window fills the bottom-left quadrant of the current monitor
0. **Bottom**<br />
   The window is minimized
0. **Bottom-right corner**<br />
   The window fills the bottom-right quadrant of the current monitor

This gridding behavior was chosen to mimic the window snapping/resizing functionality found in modern Linux desktops such as KDE and Gnome, with the addition of a minimize area.

###### Support for multiple monitors
Up to two monitors are currently supported by Perfect Grid; support for more may be added later if there's interest in such.